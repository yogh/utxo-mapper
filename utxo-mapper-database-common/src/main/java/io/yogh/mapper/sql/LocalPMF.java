package io.yogh.mapper.sql;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

import javax.naming.NamingException;
import javax.sql.DataSource;

import org.apache.commons.dbcp2.cpdsadapter.DriverAdapterCPDS;
import org.apache.commons.dbcp2.datasources.SharedPoolDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.yogh.mapper.config.ApplicationProperties;

public class LocalPMF {

  private static final Logger LOG = LoggerFactory.getLogger(LocalPMF.class);

  private static LocalPMF instance;

  private final DataSource ds;

  private LocalPMF() throws NamingException {
    final SharedPoolDataSource ds = new SharedPoolDataSource();

    final Properties props = ApplicationProperties.get();

    final DriverAdapterCPDS adapter = new DriverAdapterCPDS();
    adapter.setMaxIdle(16);
    adapter.setUrl(props.getProperty("pmf.url"));
    adapter.setUser(props.getProperty("pmf.user"));
    adapter.setPassword(props.getProperty("pmf.pass"));
    try {
      adapter.setDriver("org.postgresql.Driver");
    } catch (final ClassNotFoundException e) {
      throw new RuntimeException(e);
    }

    ds.setConnectionPoolDataSource(adapter);
    this.ds = ds;
  }

  /**
   * Returns the instance of the PMF.
   *
   * @return instance
   */
  public static LocalPMF getInstance() {
    if (instance == null) {
      try {
        instance = new LocalPMF();
      } catch (final NamingException e) {
        LOG.error("[DBManager#getConnection] NamingException. Data source for database connection isn't correct", e);
        throw new RuntimeException(e);
      }
    }
    return instance;
  }

  /**
   * Returns a new connection to the AeriusDB database.
   *
   * @return a database connection
   * @throws SQLException
   *           When retrieving a connection fails.
   */
  public Connection getConnection() throws SQLException {
    try {
      return ds.getConnection();
    } catch (final SQLException e) {
      LOG.error("[DBManager#getConnection] SQLException. Problem getting a connection.", e);
      throw e;
    }
  }
}
