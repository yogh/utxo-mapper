package io.yogh.mapper.config;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Properties;

public class ApplicationProperties {
  public static final String APPLICATION_PROPERTIES_PROP = "application.properties";

  private static ApplicationProperties instance;

  private final Properties props;

  public ApplicationProperties() {
    props = new Properties();
    try {
      props.load(Files.newInputStream(Paths.get(System.getProperty(APPLICATION_PROPERTIES_PROP))));
    } catch (final IOException e) {
      throw new RuntimeException(e);
    }
  }

  /**
   * Returns the instance of the PMF.
   *
   * @return instance
   */
  public static Properties get() {
    if (instance == null) {
      instance = new ApplicationProperties();
    }
    return instance.props;
  }
}
