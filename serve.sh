# Check if the necessary tools are installed
command -v tmux >/dev/null 2>&1 || { echo >&2 "I require tmux but it's not installed.  Aborting."; exit 1; }
command -v entr >/dev/null 2>&1 || { echo >&2 "I require entr but it's not installed.  Aborting."; exit 1; }

# Kill existing session of the same name, and sleep a while to let threads die. Do this before port checks.
tmux kill-session -t utxo-mapper-serve >/dev/null 2>&1 && { echo "Waiting for existing session to close"; sleep 2; }

# Check if 'lsof' is available, and if so do port checks
command -v lsof >/dev/null 2>&1 && { 
  lsof -i:9876 >/dev/null 2>&1 && { echo "Need port 9876 to be free. (perhaps another tmux session is hogging it? Try 'tmux kill-server'.)"; exit 1; }
  lsof -i:8080 >/dev/null 2>&1 && { echo "Need port 8080 to be free. (perhaps another tmux session is hogging it? Try 'tmux kill-server'.)"; exit 1; }
} || { echo "lsof not detected/installed. Install lsof to do port checks."; }

# Navigate to scripts, they work with relative paths
cd scripts

tmux kill-session -t utxo-mapper-serve
tmux set-option remain-on-exit 
tmux new -s utxo-mapper-serve -d ./codeserver.sh
tmux split-window -v -p 66 ./webserver.sh
tmux split-window -v ./geoserver.sh
tmux split-window -h ./livereload.sh
tmux -2 attach-session -d
