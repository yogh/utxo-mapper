CREATE TABLE bitcoin_features (
    id SERIAL PRIMARY KEY,
    name VARCHAR(64),
	geometry geometry(Polygon, 28992)
);

CREATE INDEX idx_bitcoin_features_gist ON bitcoin_features USING GIST (geometry);