package io.yogh.mapper;

import io.yogh.mapper.config.ApplicationProperties;

public class Main {
  public static void main(final String[] args) {
    if (System.getProperty(ApplicationProperties.APPLICATION_PROPERTIES_PROP) == null) {
      System.setProperty(ApplicationProperties.APPLICATION_PROPERTIES_PROP, args[0]);
    }

    Installer.create().build();
  }
}
