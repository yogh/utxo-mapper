package io.yogh.mapper;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.machinezoo.noexception.Exceptions;

import io.yogh.mapper.config.ApplicationProperties;
import io.yogh.mapper.sql.LocalPMF;

public class Installer {
  private static final Logger LOG = LoggerFactory.getLogger(Installer.class);

  private static final String CONFIG_DIR = "postgres.config.dir";

  private static final String BUILD_SKIP = "postgres.build.skip";

  private static final String LINE_SKIP_STRING = "#";
  private static final String END_QUERY_STRING = ";";

  private final LocalPMF pmf;

  private final Properties props;

  public Installer(final LocalPMF pmf) {
    this.pmf = pmf;

    props = ApplicationProperties.get();
  }

  public static Installer create() {
    return new Installer(LocalPMF.getInstance());
  }

  public void build() {

    final String path = props.getProperty(CONFIG_DIR);
    LOG.info("Installation configuration at: {}", path);

    try (final Connection con = pmf.getConnection()) {
      execute(con, Paths.get(path));
    } catch (final SQLException | IOException e) {
      throw new RuntimeException(e);
    }
  }

  private void execute(final Connection con, final Path path) throws IOException {
    if (Files.isDirectory(path)) {
      if ("build".equals(path.getFileName().toString())) {
        if ("true".equals(props.get(BUILD_SKIP))) {
          LOG.info("Encountered build path and skipping: {}", path);
//          return;
        }
      }

      Files.list(path).forEach(Exceptions.sneak().consumer(v -> execute(con, v)));
    } else {
      executeFile(con, path);
    }
  }

  private void executeFile(final Connection con, final Path path) throws IOException {
    LOG.info("Executing file: {}", path);
    final StringBuilder bldr = new StringBuilder();
    Files.lines(path).forEach(line -> {
      if (line.startsWith(LINE_SKIP_STRING)) {
        return;
      }

      bldr.append(line);

      if (line.endsWith(END_QUERY_STRING)) {
        executeQuery(con, bldr.toString());
        bldr.setLength(0);
      }
    });
  }

  private void executeQuery(final Connection con, final String query) {
    try {
      LOG.info("Executing: {}", query);
      final PreparedStatement stmt = con.prepareStatement(query);
      stmt.execute();

    } catch (final SQLException e) {
      throw new RuntimeException(e);
    }
  }
}
