package io.yogh.mapper.wui.application.service;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.inject.ImplementedBy;

import io.yogh.gwt.geo.domain.layer.IsLayerConfiguration;

@ImplementedBy(LocalLayerServiceAsync.class)
public interface LayerServiceAsync {
  void getLayer(String name, AsyncCallback<IsLayerConfiguration> callback);
}
