package io.yogh.mapper.wui.vue;

import com.google.inject.Inject;

import io.yogh.gwt.vue.StaticVueProvider;

public class VueRootProvider {
  public static class VueRoot extends StaticVueProvider<VueRootView> {
    @Inject
    public VueRoot(final VueRootViewFactory factory) {
      super(factory);
    }
  }
}
