/*
 * Copyright Dutch Ministry of Agriculture, Nature and Food Quality
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package io.yogh.mapper.wui.application.dev;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.inject.Singleton;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import io.yogh.gwt.wui.command.PlaceChangeCommand;
import io.yogh.gwt.wui.dev.DevelopmentObserver;
import io.yogh.gwt.wui.event.BasicEventComponent;
import io.yogh.gwt.wui.event.PlaceChangeEvent;
import io.yogh.gwt.wui.util.GWTProd;

@Singleton
public class ApplicationDevelopmentObserver extends BasicEventComponent implements DevelopmentObserver {
  interface DevelopmentObserverEventBinder extends EventBinder<ApplicationDevelopmentObserver> {}

  private final DevelopmentObserverEventBinder EVENT_BINDER = GWT.create(DevelopmentObserverEventBinder.class);

  // @SuppressWarnings("rawtypes")
  // @EventHandler(handles = {})
  // public void onSimpleGenericEvent(final SimpleGenericEvent e) {
  // log(e.getClass().getSimpleName(), e.getValue());
  // }
  //
  // @SuppressWarnings("rawtypes")
  // @EventHandler(handles = {})
  // public void onSimpleGenericCommand(final SimpleGenericCommand c) {
  // log(c.getClass().getSimpleName(), c.getValue());
  // }

  @EventHandler
  public void onPlaceChangeCommand(final PlaceChangeCommand e) {
    log("PlaceChangeCommand", e.getValue());
  }

  @EventHandler
  public void onPlaceChangeEvent(final PlaceChangeEvent e) {
    log("PlaceChangeEvent", e.getValue());

    Scheduler.get().scheduleDeferred(() -> brbr());
  }

  private void brbr() {
    logRaw("");
  }

  @SuppressWarnings("unused")
  private void log(final String origin) {
    logRaw("[" + origin + "]");
  }

  private void log(final String origin, final Object val) {
    logRaw("[" + origin + "] " + String.valueOf(val));
  }

  private void logRaw(final String string) {
    GWTProd.log(string);
  }

  @Override
  public void setEventBus(final EventBus eventBus) {
    super.setEventBus(eventBus, this, EVENT_BINDER);
  }
}
