package io.yogh.mapper.wui.application.activity;

import io.yogh.mapper.wui.application.place.LandingPlace;
import io.yogh.mapper.wui.application.ui.landing.LandingActivity;

public interface ApplicationActivityFactory {
  LandingActivity createLandingActivity(LandingPlace place);
}
