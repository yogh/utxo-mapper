/*
 * Copyright Dutch Ministry of Agriculture, Nature and Food Quality
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package io.yogh.mapper.wui.application;

import com.google.gwt.inject.client.assistedinject.GinFactoryModuleBuilder;
import com.google.gwt.place.shared.PlaceHistoryHandler.Historian;
import com.google.inject.TypeLiteral;

import io.yogh.gwt.vue.AcceptsOneComponent;
import io.yogh.gwt.vue.activity.VueActivityManager;
import io.yogh.gwt.wui.activity.ActivityManager;
import io.yogh.gwt.wui.activity.ActivityMapper;
import io.yogh.gwt.wui.daemon.DaemonBootstrapper;
import io.yogh.gwt.wui.dev.DevelopmentObserver;
import io.yogh.gwt.wui.history.HTML5Historian;
import io.yogh.gwt.wui.history.PlaceHistoryMapper;
import io.yogh.gwt.wui.place.ApplicationPlace;
import io.yogh.gwt.wui.place.DefaultPlace;
import io.yogh.mapper.wui.application.activity.ApplicationActivityFactory;
import io.yogh.mapper.wui.application.activity.ApplicationActivityMapper;
import io.yogh.mapper.wui.application.activity.ApplicationPlaceHistoryMapper;
import io.yogh.mapper.wui.application.daemon.ApplicationDaemonBootstrapper;
import io.yogh.mapper.wui.application.dev.ApplicationDevelopmentObserver;
import io.yogh.mapper.wui.application.place.LandingPlace;

public class ApplicationClientModule extends BareClientModule {
  @Override
  protected void configure() {
    super.configure();
    bind(ApplicationPlace.class).annotatedWith(DefaultPlace.class).to(LandingPlace.class);
    bind(Historian.class).to(HTML5Historian.class);

    bind(new TypeLiteral<ActivityManager<AcceptsOneComponent>>() {}).to(VueActivityManager.class);
    bind(new TypeLiteral<ActivityMapper<AcceptsOneComponent>>() {}).to(ApplicationActivityMapper.class);
    bind(DaemonBootstrapper.class).to(ApplicationDaemonBootstrapper.class);
    bind(PlaceHistoryMapper.class).to(ApplicationPlaceHistoryMapper.class);
    bind(DevelopmentObserver.class).to(ApplicationDevelopmentObserver.class);

    // Bind factories
    install(new GinFactoryModuleBuilder().build(ApplicationActivityFactory.class));
  }
}
