/*
 * Copyright Dutch Ministry of Agriculture, Nature and Food Quality
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package io.yogh.mapper.wui.application.activity;

import com.google.inject.Inject;

import io.yogh.gwt.vue.AcceptsOneComponent;
import io.yogh.gwt.wui.activity.Activity;
import io.yogh.gwt.wui.activity.ActivityMapper;
import io.yogh.gwt.wui.place.Place;
import io.yogh.mapper.wui.application.place.LandingPlace;

public class ApplicationActivityMapper implements ActivityMapper<AcceptsOneComponent> {
  private final ApplicationActivityFactory factory;

  @Inject
  public ApplicationActivityMapper(final ApplicationActivityFactory factory) {
    this.factory = factory;
  }

  @Override
  public Activity<?, AcceptsOneComponent> getActivity(final Place place) {
    Activity<?, AcceptsOneComponent> presenter = null;

    presenter = tryGetActivity(place);

    if (presenter == null) {
      throw new RuntimeException("Presenter is null: Place ends up nowhere. " + place);
    }

    return presenter;
  }

  private Activity<?, AcceptsOneComponent> tryGetActivity(final Place place) {
    if (place instanceof LandingPlace) {
      return factory.createLandingActivity((LandingPlace) place);
    }

    return null;
  }
}
