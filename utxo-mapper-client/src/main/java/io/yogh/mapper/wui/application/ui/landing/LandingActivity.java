package io.yogh.mapper.wui.application.ui.landing;

import com.google.inject.Inject;

import io.yogh.gwt.vue.activity.AbstractVueActivity;

public class LandingActivity extends AbstractVueActivity<LandingActivity, LandingView, LandingViewFactory> {
  @Inject
  public LandingActivity() {
    super(LandingViewFactory.get());
  }

  @Override
  public LandingActivity getPresenter() {
    return this;
  }
}
