package io.yogh.mapper.wui.vue;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.client.component.IsVueComponent;
import com.axellience.vuegwt.core.client.vue.VueComponentFactory;
import com.google.web.bindery.event.shared.EventBus;

import io.yogh.gwt.vue.AcceptsOneComponent;
import io.yogh.gwt.wui.event.HasEventBus;
import io.yogh.mapper.wui.application.ui.landing.LandingView;

@Component(components = {
    LandingView.class
})
public class VueRootView implements IsVueComponent, AcceptsOneComponent, HasEventBus {
  @Data String id;
  @Data Object presenter;

  @Data EventBus eventBus;

  @Override
  public <P> void setComponent(final VueComponentFactory<?> factory, final P presenter) {
    id = factory.getComponentTagName();
    this.presenter = presenter;
  }

  @Override
  public void setEventBus(final EventBus eventBus) {
    this.eventBus = eventBus;
  }
}
