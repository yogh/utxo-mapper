package io.yogh.mapper.wui.application.place;

import io.yogh.gwt.wui.place.ApplicationPlace;

public class LandingPlace extends ApplicationPlace {
  public LandingPlace() {
    super(ApplicationTokenizers.LANDING);
  }
}
