package io.yogh.mapper.wui.application.daemon;

import com.google.web.bindery.event.shared.EventBus;

import io.yogh.gwt.wui.daemon.DaemonBootstrapper;

public class ApplicationDaemonBootstrapper implements DaemonBootstrapper {
  @Override
  public void setEventBus(final EventBus eventBus) {

  }

  @Override
  public void init(final Runnable complete) {

  }
}
