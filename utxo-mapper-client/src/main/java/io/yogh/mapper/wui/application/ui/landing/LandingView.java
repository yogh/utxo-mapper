package io.yogh.mapper.wui.application.ui.landing;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.client.component.IsVueComponent;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;
import com.google.web.bindery.event.shared.EventBus;

import io.yogh.gwt.geo.command.LayerAddedCommand;
import io.yogh.gwt.geo.util.MapLayerUtil;
import io.yogh.gwt.geo.wui.OL3MapComponent;
import io.yogh.gwt.wui.service.AppAsyncCallback;
import io.yogh.mapper.wui.application.service.LayerServiceAsync;

@Component(components = {
    OL3MapComponent.class
})
public class LandingView implements IsVueComponent, HasCreated {
  @Prop LandingActivity presenter;

  @Prop EventBus eventBus;

  @Inject LayerServiceAsync layerService;

  @Override
  public void created() {
    layerService.getLayer("tester",
        AppAsyncCallback.create(layer -> eventBus.fireEvent(new LayerAddedCommand(MapLayerUtil.prepareLayer(layer, eventBus)))));
  }
}
