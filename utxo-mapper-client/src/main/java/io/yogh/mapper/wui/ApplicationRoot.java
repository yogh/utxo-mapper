/*
 * Copyright Dutch Ministry of Agriculture, Nature and Food Quality
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package io.yogh.mapper.wui;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;

import com.google.gwt.core.client.GWT;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.IncompatibleRemoteServiceException;
import com.google.gwt.user.client.rpc.InvocationException;
import com.google.gwt.user.client.rpc.StatusCodeException;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;

import io.yogh.gwt.geo.GeoInitializer;
import io.yogh.gwt.vue.AcceptsOneComponent;
import io.yogh.gwt.wui.activity.ActivityManager;
import io.yogh.gwt.wui.daemon.DaemonBootstrapper;
import io.yogh.gwt.wui.dev.DevelopmentObserver;
import io.yogh.gwt.wui.event.BasicEventComponent;
import io.yogh.gwt.wui.event.RequestClientLoadFailureEvent;
import io.yogh.gwt.wui.event.RequestConnectionFailureEvent;
import io.yogh.gwt.wui.event.RequestServerLoadFailureEvent;
import io.yogh.gwt.wui.history.HistoryManager;
import io.yogh.gwt.wui.service.RequestBlockedException;
import io.yogh.gwt.wui.service.RequestClientException;
import io.yogh.gwt.wui.service.RequestServerException;
import io.yogh.gwt.wui.util.SchedulerUtil;
import io.yogh.gwt.wui.util.WebUtil;
import io.yogh.mapper.wui.application.i18n.M;
import io.yogh.mapper.wui.vue.VueRootView;
import io.yogh.mapper.wui.vue.VueRootViewFactory;

/**
 * Root of the application logic.
 */
public class ApplicationRoot extends BasicEventComponent {
  private final ApplicationRootEventBinder EVENT_BINDER = GWT.create(ApplicationRootEventBinder.class);

  interface ApplicationRootEventBinder extends EventBinder<ApplicationRoot> {}

  @SuppressWarnings("unused") @Inject private DevelopmentObserver development;

  @Inject private HistoryManager historyManager;

  @Inject private EventBus eventBus;

  @Inject DaemonBootstrapper daemonBootstrapper;

  @Inject ActivityManager<AcceptsOneComponent> activityManager;

  @Inject VueRootViewFactory rootViewFactory;

  @Inject GeoInitializer geoInitializer;

  private VueRootView rootView;

  /**
   * Starts the application.
   *
   * @param bootstrapper
   * @param loginFirst
   */
  public void startUp() {
    WebUtil.setAbsoluteRoot("/");

    setUncaughtExceptionHandler();

    daemonBootstrapper.setEventBus(eventBus);

    rootView = rootViewFactory.create();
    activityManager.setPanel(rootView);

    SchedulerUtil.delay(this::onFinishStartup, 500);
  }

  private void onFinishStartup() {
    rootView.vue().$mount("#base");

    historyManager.handleCurrentHistory();
  }

  /**
   * Hides the main application display, if attached.
   */
  public void hideDisplay() {
    // rootView.setComponent("error");
  }

  private void setUncaughtExceptionHandler() {
    GWT.setUncaughtExceptionHandler(e -> {
      final Throwable cause = findCause(e);

      if (cause instanceof IncompatibleRemoteServiceException && Window.confirm(M.messages().errorInternalApplicationOutdated())) {
        Window.Location.reload();
        return;
      }

      if (!isKnownException(cause)) {
        Logger.getLogger("UncaughtExceptionHandler").log(Level.SEVERE, cause.getMessage(), cause);
      } else {
        if (cause instanceof RequestClientException) {
          eventBus.fireEvent(new RequestClientLoadFailureEvent(true));
        } else if (cause instanceof RequestServerException) {
          eventBus.fireEvent(new RequestServerLoadFailureEvent(true));
        } else if (cause instanceof RequestBlockedException) {
          eventBus.fireEvent(new RequestConnectionFailureEvent(true));
        }
      }
    });
  }

  private Throwable findCause(final Throwable e) {
    if (e == null) {
      return null;
    }

    if (e.getCause() != null) {
      return findCause(e.getCause());
    } else {
      return e;
    }
  }

  private boolean isKnownException(final Throwable e) {
    return e instanceof IncompatibleRemoteServiceException
        || e instanceof InvocationException
        || e instanceof StatusCodeException
        || e instanceof RequestException;
  }

  @Override
  public void setEventBus(final EventBus eventBus) {
    super.setEventBus(eventBus, this, EVENT_BINDER);
  }
}
