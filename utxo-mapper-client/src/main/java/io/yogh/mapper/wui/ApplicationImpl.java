/*
 * Copyright Dutch Ministry of Agriculture, Nature and Food Quality
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package io.yogh.mapper.wui;

import com.google.inject.Inject;

import io.yogh.mapper.wui.application.resources.R;

public class ApplicationImpl extends Application {
  @Inject private ApplicationRoot appRoot;

  @Override
  public void create() {
    R.init();

    ApplicationGinjector.INSTANCE.inject(this);

    onFinishedLoading();
  }

  /**
   * Initializes the application activity managers, user interface, and starts the application by handling the current history token.
   */
  private void onFinishedLoading() {
    try {
      appRoot.startUp();
    } catch (final Exception e) {
      appRoot.hideDisplay();
      throw e;
    }
  }
}
