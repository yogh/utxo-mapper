package io.yogh.mapper.wui.application.service;

import java.util.HashMap;
import java.util.Map;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.inject.Singleton;

import io.yogh.gwt.geo.domain.layer.IsLayerConfiguration;
import io.yogh.gwt.geo.domain.layer.LayerConfiguration;
import io.yogh.gwt.geo.domain.layer.WMSConfiguration;
import io.yogh.gwt.wui.util.SchedulerUtil;

@Singleton
public class LocalLayerServiceAsync implements LayerServiceAsync {
  private static final String HOST = "http://localhost:8084/";
  private static final String NAMESPACE = "/utxo/wms";

  private static final String SVG = "image/svg";

  public Map<String, IsLayerConfiguration> cache = new HashMap<>();

  public LocalLayerServiceAsync() {
    final WMSConfiguration conf = WMSConfiguration.builder()
        .layer(LayerConfiguration.builder()
            .name("tester")
            .title("Test title")
            .build())
        .formats(SVG)
        .baseLayer("utxo:bitcoin_features")
        .url(HOST + NAMESPACE)
        .viewParams("")
        .cqlFilter("")
        .baseStyle("")
        .build();

    cache.put(conf.layer().name(), conf);
  }

  @Override
  public void getLayer(final String name, final AsyncCallback<IsLayerConfiguration> callback) {
    SchedulerUtil.delay(() -> callback.onSuccess(cache.get(name)));
  }
}
