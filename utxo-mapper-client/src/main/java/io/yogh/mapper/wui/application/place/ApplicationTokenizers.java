package io.yogh.mapper.wui.application.place;

import io.yogh.gwt.wui.place.ApplicationPlace;
import io.yogh.gwt.wui.place.PlaceTokenizer;

public class ApplicationTokenizers {
  private static final String LANDING_KEY = "landing";

  public static final PlaceTokenizer<LandingPlace> LANDING = new ApplicationPlace.Tokenizer<>(
      () -> new LandingPlace(), LANDING_KEY);
}
