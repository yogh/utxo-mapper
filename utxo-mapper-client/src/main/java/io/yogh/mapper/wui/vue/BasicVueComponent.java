package io.yogh.mapper.wui.vue;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.client.component.IsVueComponent;

import io.yogh.mapper.wui.application.i18n.ApplicationMessages;
import io.yogh.mapper.wui.application.i18n.M;
import io.yogh.mapper.wui.application.resources.ImageResources;
import io.yogh.mapper.wui.application.resources.R;

@Component(hasTemplate = false)
public abstract class BasicVueComponent implements IsVueComponent {
  @Data public ApplicationMessages i18n = M.messages();

  @Data public ImageResources img = R.images();

  public void destroy() {
    vue().$destroy();
    vue().$el().parentNode.removeChild(vue().$el());
  }
}
