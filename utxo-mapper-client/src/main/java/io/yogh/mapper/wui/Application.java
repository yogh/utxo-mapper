package io.yogh.mapper.wui;

import com.google.gwt.core.client.GWT;

public class Application {
  public static final Application A = GWT.create(Application.class);

  public void create() {
    throw new RuntimeException("No Application implementation injected.");
  }
}
