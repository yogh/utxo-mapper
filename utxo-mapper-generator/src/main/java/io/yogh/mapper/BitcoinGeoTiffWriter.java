package io.yogh.mapper;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.geotools.coverage.grid.GridCoverage2D;
import org.geotools.coverage.grid.GridCoverageFactory;
import org.geotools.gce.geotiff.GeoTiffWriter;
import org.geotools.geometry.Envelope2D;
import org.geotools.referencing.CRS;
import org.opengis.geometry.Envelope;
import org.opengis.referencing.FactoryException;
import org.opengis.referencing.crs.CoordinateReferenceSystem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BitcoinGeoTiffWriter {
  private static final Logger LOG = LoggerFactory.getLogger(BitcoinGeoTiffWriter.class);

  private static final String CRS_STRING = "PROJCS[\"Amersfoort / RD New\",GEOGCS[\"Amersfoort\",DATUM[\"Amersfoort\",SPHEROID[\"Bessel 1841\",6377397.155,299.1528128,AUTHORITY[\"EPSG\",\"7004\"]],AUTHORITY[\"EPSG\",\"6289\"]],PRIMEM[\"Greenwich\",0,AUTHORITY[\"EPSG\",\"8901\"]],UNIT[\"degree\",0.01745329251994328,AUTHORITY[\"EPSG\",\"9122\"]],AUTHORITY[\"EPSG\",\"4289\"]],UNIT[\"metre\",1,AUTHORITY[\"EPSG\",\"9001\"]],PROJECTION[\"Oblique_Stereographic\"],PARAMETER[\"latitude_of_origin\",52.15616055555555],PARAMETER[\"central_meridian\",5.38763888888889],PARAMETER[\"scale_factor\",0.9999079],PARAMETER[\"false_easting\",155000],PARAMETER[\"false_northing\",463000],AUTHORITY[\"EPSG\",\"28992\"],AXIS[\"X\",EAST],AXIS[\"Y\",NORTH]]";

  private CoordinateReferenceSystem crs;

  private final Path target;

  public BitcoinGeoTiffWriter(final String targetDir) {
    target = Paths.get(targetDir);
    try {
      crs = CRS.parseWKT(CRS_STRING);
    } catch (final FactoryException e) {
      e.printStackTrace();
    }
  }

  public void write(final BufferedImage img, final String name, final int x, final int y, final int scale) {
    try {
      final File file = target.resolve(name + ".tif").toFile();
      final GeoTiffWriter writer = new GeoTiffWriter(file);

      LOG.info("Writing GeoTiff to {}", file.toString());

      write(writer, name, x, y, img, scale);
    } catch (final IOException e1) {
      e1.printStackTrace();
    }
  }

  private void write(final GeoTiffWriter writer, final String name, final double x, final double y, final BufferedImage bi, final int scale) {
    final Envelope envelope = new Envelope2D(crs, x, y, bi.getWidth() * scale, bi.getHeight() * scale);
    final GridCoverageFactory factory = new GridCoverageFactory();
    final GridCoverage2D test = factory.create(name, bi, envelope);

    try {
      writer.write(test, null);
      writer.dispose();
    } catch (final IllegalArgumentException e) {
      e.printStackTrace();
    } catch (final IndexOutOfBoundsException e) {
      e.printStackTrace();
    } catch (final IOException e) {
      e.printStackTrace();
    }
  }

}
