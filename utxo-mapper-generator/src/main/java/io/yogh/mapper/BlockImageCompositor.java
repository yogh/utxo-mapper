package io.yogh.mapper;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.font.FontRenderContext;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.awt.image.IndexColorModel;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;

import io.yogh.mapper.domain.BitcoinGeoInformation;
import io.yogh.mapper.domain.Block;
import io.yogh.mapper.domain.GeoBufferedImage;

public class BlockImageCompositor {
  private static final int ALIGNER_SIZE = 30;
  private static final int HUNDRED = 100;
  private static final int THOUSAND = 1000;

  public static final int WIDTH = 2 * THOUSAND;
  public static final int HEIGHT = 10 * THOUSAND;

  private static final int LABEL_MARGIN_LEFT = 100;

  private static final Color COLOR_1 = new Color(246, 250, 255);
  private static final Color COLOR_2 = new Color(238, 234, 24);
  private static final Color COLOR_3 = new Color(237, 196, 149);
  private static final Color COLOR_4 = new Color(61, 156, 9);
  private static final Color COLOR_5 = new Color(8, 145, 205);
  private static final Color COLOR_6 = new Color(119, 107, 125);
  private static final Color COLOR_7 = new Color(230, 40, 0);
  private static final Color COLOR_8 = new Color(0, 8, 0);

  private static final Color BLACK = COLOR_8;
  private static final Color GREEN = COLOR_4;
  private static final Color BLUE = COLOR_5;
  private static final Color RED = COLOR_7;

  private static final Color TINT = COLOR_6;

  private static final Color[] PALETTE = new Color[] { COLOR_1, COLOR_2, COLOR_3, COLOR_4,
      COLOR_5, COLOR_6, COLOR_7, COLOR_8 };

  public static GeoBufferedImage composeBlockStrip(final Block block) {
    final IndexColorModel palette = PaletteBuilder.build(PALETTE);

    final BufferedImage bi = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_BYTE_INDEXED, palette);
    final Graphics2D graph = getGraph(bi);

    final int titleHeight = 500;
    final int idHeight = 650;
    final int delimiterHeight = 750;
    final int timeHeight = 950;

    // dummyCorners(graph);

    final Font titleFont = new Font("VCR OSD Mono", Font.BOLD, 512);
    final Font textFont = new Font("VCR OSD Mono", Font.PLAIN, 48);
    final DateTimeFormatter formatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.LONG)
        .withLocale(Locale.UK)
        .withZone(ZoneId.systemDefault());

    // Draw the block height title
    graph.setColor(BLUE);
    graph.setFont(titleFont);
    final int blockHeightWidth = graph.getFontMetrics().stringWidth(String.valueOf(block.height()));
    graph.drawString(String.valueOf(block.height()), WIDTH / 2 - blockHeightWidth / 2, titleHeight);

    // Draw the block ID
    graph.setColor(BLUE);
    graph.setFont(textFont);
    final int blockIdWidth = graph.getFontMetrics().stringWidth(String.valueOf(block.id()));
    graph.drawString(String.valueOf(block.id()), WIDTH / 2 - blockIdWidth / 2, idHeight);

    // Draw delimiter
    graph.setColor(TINT);
    graph.fillRect(0, delimiterHeight, WIDTH, 50);

    // Draw time string
    graph.setColor(TINT);
    graph.setFont(textFont);
    graph.drawString("Time:", 100, timeHeight);
    graph.setColor(RED);

    final String blockTime = formatter.format(Instant.ofEpochSecond(block.time()));
    final int blockTimeWidth = graph.getFontMetrics().stringWidth(blockTime);
    graph.drawString(blockTime, WIDTH / 2 - blockTimeWidth / 2, timeHeight);

    graph.fillOval(WIDTH / 2 - (WIDTH / 4), HEIGHT - WIDTH, WIDTH / 2, WIDTH / 2);

    return GeoBufferedImage.builder()
        .geo(BitcoinGeoInformation.builder()
            .height(block.height())
            .name("block-{height}-strip")
            .build())
        .image(bi)
        .build();
  }

  public static GeoBufferedImage composeBlockLabel(final int height) {
    final IndexColorModel palette = PaletteBuilder.build(PALETTE);

    final Font font = new Font("VCR OSD Mono", Font.BOLD, 512);

    final String text = String.valueOf(height);
    final AffineTransform affinetransform = new AffineTransform();
    final FontRenderContext frc = new FontRenderContext(affinetransform, true, true);

    System.out.println(font.getStringBounds(text, frc).getWidth());

    final int textWidth = (int) (font.getStringBounds(text, frc).getWidth());
    final int textHeight = (int) (font.getStringBounds(text, frc).getHeight());

    System.out.println("Font dims: " + textWidth + ":" + textHeight);

    final BufferedImage bi = new BufferedImage(textWidth, textHeight, BufferedImage.TYPE_BYTE_INDEXED, palette);
    final Graphics2D graph = getGraph(bi);

    graph.setFont(font);
    graph.setColor(BLUE);
    graph.drawString(String.valueOf(height), 0, textHeight);

    return GeoBufferedImage.builder()
        .geo(BitcoinGeoInformation.builder()
            .height(height)
            .name("block-{height}-label")
            .build())
        .image(bi)
        .build();
  }

  private static Graphics2D getGraph(final BufferedImage bi) {
    final Graphics2D graph = bi.createGraphics();
    graph.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
    graph.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
    graph.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
    return graph;
  }
}
