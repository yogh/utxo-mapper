package io.yogh.mapper;

import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.GraphicsEnvironment;
import java.io.IOException;
import java.io.InputStream;

import io.yogh.mapper.domain.Block;
import io.yogh.mapper.domain.GeoPositioner;

public class Generator {
  public static void main(final String[] args) {
    initFonts();

    final BitcoinGeoTiffWriter tiffWriter = new BitcoinGeoTiffWriter(args[0]);

    final var writer = SimpleGeoBlockWriter.builder()
        .geoTiffWriter(tiffWriter)
        .build();

    final int limit = 101;

    for (int i = 0; i < limit; i++) {
      writeBlock(writer, i);
    }
  }

  private static void initFonts() {
    registerFont("VCR_OSD_MONO_1.001.ttf");
  }

  private static void registerFont(final String ttf) {
    final InputStream str = Generator.class.getClassLoader().getResourceAsStream(ttf);
    Font font;
    try {
      font = Font.createFont(Font.TRUETYPE_FONT, str);
    } catch (FontFormatException | IOException e) {
      throw new RuntimeException(e);
    }
    GraphicsEnvironment.getLocalGraphicsEnvironment().registerFont(font);
    System.out.println("Registered font: " + font.getName());
  }

  private static void writeBlock(final SimpleGeoBlockWriter writer, final int height) {
    final Block block = fetchBlock(height);

    // Write the block strip
    writer.accept(GeoPositioner.builder()
        .x(height * BlockImageCompositor.WIDTH)
        .build(),
        BlockImageCompositor.composeBlockStrip(block));

  }

  private static Block fetchBlock(final int height) {
    return Block.builder()
        .height(height)
        .id("000000000000000000170a0551c8f69e58d71efe2d8f2743adfdec780fd67dfc")
        .time(1231006505L)
        .build();
  }
}
