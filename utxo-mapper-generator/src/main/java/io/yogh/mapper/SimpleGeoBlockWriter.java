package io.yogh.mapper;

import com.google.auto.value.AutoValue;

import io.yogh.mapper.domain.BitcoinGeoInformation;
import io.yogh.mapper.domain.GeoBufferedImage;
import io.yogh.mapper.domain.GeoPositioner;
import io.yogh.mapper.domain.IsBitcoinGeoImage;

@AutoValue
public abstract class SimpleGeoBlockWriter {
  public static Builder builder() {
    return new AutoValue_SimpleGeoBlockWriter.Builder();
  }

  abstract BitcoinGeoTiffWriter geoTiffWriter();

  @AutoValue.Builder
  public static abstract class Builder {
    public abstract Builder geoTiffWriter(BitcoinGeoTiffWriter value);

    public abstract SimpleGeoBlockWriter build();
  }

  public void accept(final GeoPositioner positioner, final GeoBufferedImage img) {
    final String formattedName = formatName(img.geo());

    geoTiffWriter().write(img.image(), formattedName, positioner.x(), positioner.y(), positioner.scale());
  }
  public void accept(final GeoPositioner positioner, final IsBitcoinGeoImage img) {
    throw new RuntimeException("No support for geo image type: " + img.getClass().getSimpleName());
  }

  private String formatName(final BitcoinGeoInformation geo) {
    return geo.name()
        .replace("{height}", padLeft(String.valueOf(geo.height()), 6));
  }

  private String padLeft(final String inp, final int length) {
    return String.format("%1$" + length + "s", inp).replace(' ', '0');
  }
}
