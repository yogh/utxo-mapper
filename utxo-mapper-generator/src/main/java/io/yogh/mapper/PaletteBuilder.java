package io.yogh.mapper;

import java.awt.Color;
import java.awt.image.IndexColorModel;

public class PaletteBuilder {
  private static final int BITS = 3;
  private static final int COLORS = 8;

  public static IndexColorModel build(final Color[] colors) {
    assert colors.length == 8;

    return build(colors[0], colors[1], colors[2], colors[3], colors[4], colors[5], colors[6], colors[7]);
  }

  public static IndexColorModel build(final Color col1, final Color col2, final Color col3, final Color col4,
      final Color col5, final Color col6, final Color col7, final Color col8) {
    return new IndexColorModel(BITS, COLORS,
        new byte[] { (byte) col1.getRed(), (byte) col2.getRed(), (byte) col3.getRed(), (byte) col4.getRed(),
            (byte) col5.getRed(), (byte) col6.getRed(), (byte) col7.getRed(), (byte) col8.getRed() },
        new byte[] { (byte) col1.getGreen(), (byte) col2.getGreen(), (byte) col3.getGreen(), (byte) col4.getGreen(),
            (byte) col5.getGreen(), (byte) col6.getGreen(), (byte) col7.getGreen(), (byte) col8.getGreen() },
        new byte[] { (byte) col1.getBlue(), (byte) col2.getBlue(), (byte) col3.getBlue(), (byte) col4.getBlue(),
            (byte) col5.getBlue(), (byte) col6.getBlue(), (byte) col7.getBlue(), (byte) col8.getBlue() });
  }
}
