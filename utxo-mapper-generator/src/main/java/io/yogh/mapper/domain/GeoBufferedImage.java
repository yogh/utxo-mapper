package io.yogh.mapper.domain;

import java.awt.image.BufferedImage;

import com.google.auto.value.AutoValue;

@AutoValue
public abstract class GeoBufferedImage implements IsBitcoinGeoImage {
  public static Builder builder() {
    return new AutoValue_GeoBufferedImage.Builder();
  }

  @Override
  public abstract BitcoinGeoInformation geo();

  public abstract BufferedImage image();

  @AutoValue.Builder
  public static abstract class Builder {
    public abstract Builder image(BufferedImage value);

    public abstract Builder geo(BitcoinGeoInformation value);

    public abstract GeoBufferedImage build();
  }
}
