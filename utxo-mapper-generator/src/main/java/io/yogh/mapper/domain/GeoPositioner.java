package io.yogh.mapper.domain;

import com.google.auto.value.AutoValue;

@AutoValue
public abstract class GeoPositioner {
  public static Builder builder() {
    return new AutoValue_GeoPositioner.Builder()
        .y(0)
        .x(0)
        .scale(1);
  }

  public abstract int x();

  public abstract int y();

  public abstract int scale();

  @AutoValue.Builder
  public static abstract class Builder {
    public abstract Builder x(int value);

    public abstract Builder y(int value);

    public abstract Builder scale(int value);

    public abstract GeoPositioner build();
  }
}
