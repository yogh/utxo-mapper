package io.yogh.mapper.domain;

import com.google.auto.value.AutoValue;

@AutoValue
public abstract class BitcoinGeoInformation {
  public static Builder builder() {
    return new AutoValue_BitcoinGeoInformation.Builder();
  }

  public abstract int height();

  public abstract String name();

  @AutoValue.Builder
  public static abstract class Builder {
    public abstract Builder height(int value);

    public abstract Builder name(String value);

    public abstract BitcoinGeoInformation build();
  }
}
