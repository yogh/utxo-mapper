package io.yogh.mapper.domain;

import com.google.auto.value.AutoValue;

@AutoValue
public abstract class Block {
  public static Builder builder() {
    return new AutoValue_Block.Builder();
  }

  public abstract String id();

  public abstract long time();

  public abstract int height();

  @AutoValue.Builder
  public static abstract class Builder {
    public abstract Builder id(String value);

    public abstract Builder time(long value);

    public abstract Builder height(int value);

    public abstract Block build();
  }
}
