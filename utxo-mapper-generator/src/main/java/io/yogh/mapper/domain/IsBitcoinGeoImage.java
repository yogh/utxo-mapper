package io.yogh.mapper.domain;

public interface IsBitcoinGeoImage {
  BitcoinGeoInformation geo();
}
