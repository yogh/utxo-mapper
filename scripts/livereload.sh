cd ..

echo "Waiting for file changes.."
ulimit -n 16384

while true; do
#find */src -regextype posix-egrep -regex ".*View.*|.*Component.*|.*scss$" | entr -d sh scripts/reload.sh target/gwt/launcherDir/reload
ag -l | entr -d sh scripts/reload.sh target/gwt/launcherDir/reload
done
