lsof -i:8084 >/dev/null 2>&1 && { echo "Need port 8084 to be free."; exit 1; }

cd ..
cd utxo-mapper-geoserver
mvn jetty:run -pl geoserver-core -am -Djetty.port=8084

