lsof -i:8080 >/dev/null 2>&1 && { echo "Need port 8080 to be free."; exit 1; }

cd ..
mvn jetty:run -pl utxo-mapper-server -am -Denv=dev

